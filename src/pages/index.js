// Step 1: Import React
import * as React from "react";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";

// Step 2: Define your component
const IndexPage = () => {
  return (
    <Layout pageTitle="Home Page">
      <p>I'm making this by following the Gatsby Tutorial.</p>
      <p>
        <b>The guy in the picture is Paddy "the Baddy" Pimblett.</b>
      </p>
      <StaticImage
        alt="Welcome to fight club"
        src="../images/paddy-the-baddy.jpg"
      />
    </Layout>
  );
};
// Step 3: Export your component
export default IndexPage;
